import mysql.connector
from datetime import date, datetime

class Database():
    # definição do banco de dados
    def __init__(self, user, password, host, database):
        self.cnx = mysql.connector.connect(user    = user,
                                          password = password,
                                          host     = host,
                                          database = database)
        self.create_tables()

    def executar(self, comando, parametros):
        cs = self.cnx.cursor()
        cs.execute(comando, parametros)
        self.cnx.commit()
        cs.close()

    def consultar(self, comando, parametros):
        cs = self.cnx.cursor()
        cs.execute(comando, parametros)
        result = []
        for i in cs:
            result.append(i)
        return result

    def __del__(self):
        self.cnx.close()
    
    def create_tables(self):
        tables = ("""
        CREATE TABLE IF NOT EXISTS tb_cliente(
            idt_cliente             INT PRIMARY KEY,
            primeiro_nome_cliente   VARCHAR(20) NOT NULL,
            ultimo_nome_cliente     VARCHAR(20) NOT NULL
        );"""
        ,"""
        CREATE TABLE IF NOT EXISTS tb_agendamento(
            idt_agendamento     INT AUTO_INCREMENT PRIMARY KEY,
            dti_agendamento     DATETIME NOT NULL,
            num_duracao         INT NOT NULL,
            cod_cliente         INT NOT NULL,
            CONSTRAINT fk_cliente_agendamento FOREIGN KEY (cod_cliente)
                REFERENCES tb_cliente(idt_cliente)
        );"""
        )

        for table in tables:
            self.executar(table, ())
    
    
    # métodos para o bot
    
    def get_next_months_names(self): # retorna uma lista, opções que vão aparecer no teclado
        dia = date.today()
        meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
        lista = []
        k, x = 1, 1
        while k < 3:
            if x == dia.month or 0 < x - dia.month < 3:
                lista.append(meses[x - 1])
                k += 1
            x += 1
        return lista


    def get_month_number(self, month):
        months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
        return months.index(month) + 1


    def get_year_from_month(self, month): # recebe o mês como número[1-12]. assume que os únicos meses possiveis são os 3 seguintes
        now = datetime.now()
        current_year = int(now.strftime("%Y"))
        current_month = int(now.strftime("%m"))
        if current_month <= month: # verifica se está dentro do mesmo ano
            return current_year
        return current_year + 1


    def get_next_days_names(self, month): # retorna uma lista, opções que vão aparecer no teclado    
        def days_with_names(days, month):
            days_ptbr = ("segunda", "terça", "quarta", "quinta", "sexta")
            month = self.get_month_number(month)
            year = self.get_year_from_month(month)
            
            for i in range(len(days)-1, -1, -1): # itera a lista do fim para o início, pq ela muda de tamanho durante a iteração e isso faria dar erro
                day_name = datetime(year, month, days[i]).strftime("%A")
                if day_name == "Saturday" or day_name == "Sunday": # exclui da lista os dias que estão no fim de semana
                    days.pop(i)
                else:
                    day_name = days_ptbr[ int(datetime(year, month, days[i]).strftime("%w")) - 1] #traduz o nome do dia
                    days[i] = "{}, {}".format(days[i], day_name) # por exemplo, 2 passa a ser "2, quinta"

            return days

        
        now = datetime.now()
        if self.get_month_number(month) == int(now.strftime("%m")): # se estiver no mês atual, começa do dia atual. Senão, começa do dia 1 mesmo
            first_day = int(datetime.now().strftime("%d"))
        else:
            first_day = 1

        days31 = ["Janeiro", "Março", "Maio", "Julho", "Agosto", "Outubro", "Dezembro"]
        days30 = [ "Abril", "Junho", "Setembro", "Novembro"]
        if month in days31:
            days = [day for day in range(first_day, 32)]
        elif month in days30:
            days = [day for day in range(first_day, 31)]
        elif month == "Fevereiro" and int(datetime.now().strftime("%Y")) % 4 == 0:
            days = [day for day in range(first_day, 30)]
        elif month == "Fevereiro":
            days = [day for day in range(first_day, 29)]
        return days_with_names(days, month)

            
    def get_available_hours(self, month, day): # retorna uma lista, opções que vão aparecer no teclado
        horas = [13, 14, 15, 16, 17, 18]
        hora_agr = datetime.datetime.now().hour
        return [i for i in horas if i > hora_agr]

    def consult_appointments(self, client_id): # retorna uma lista de agendamentos com a determinada idt
        sql = "SELECT idt_agendamento, dti_agendamento from tb_agendamento WHERE cod_cliente=%s ORDER BY dti_agendamento;"
        return self.consultar(sql, [client_id])
        
    def in_database(self, client_id): # retorna booleano, se existe ou não cliente no banco de dados
        # sql simples procurando o mesmo idt na tabela cliente
        sql = "SELECT * FROM tb_cliente WHERE idt_cliente = %s;"
        # executando sql levando como parâmetro o client_id
        result = self.consultar(sql, (client_id,))
        # retorna o boleano 
        return result != []


    def add_client_to_database(self, client_id, first_name, last_name): # adicionar linha na tabela cliente
        sql = "INSERT INTO tb_cliente(idt_cliente, primeiro_nome_cliente, ultimo_nome_cliente) VALUES(%s, %s, %s);"
        self.executar(sql, (client_id, first_name, last_name))


    def add_appointment(self, month, day, hour, client_id):  # adicionar linha na tabela agendamento, convertendo os valores pro formato do sql
        month = self.get_month_number(month)
        year = self.get_year_from_month(month)
        date = "{}-{}-{} {}:00".format(year, month, day, hour)
        
        sql = "INSERT INTO tb_agendamento(dti_agendamento, num_duracao, cod_cliente) VALUES(%s, %s, %s);"
        self.executar(sql, (date, 60, client_id))

    
    # exclui o agendamento que possui idt com o parâmetro passado
    def exclude_appointment(self, appointment_id):
        sql = "DELETE FROM tb_agendamento WHERE idt_agendamento=%s;"
        self.executar(sql, (appointment_id,))