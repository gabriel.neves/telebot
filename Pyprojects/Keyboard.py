import telebot
from telebot import types
from Messages import *
from Database import Database

# novo token com o nome genérico @nome_do_empreendimento
bot = telebot.TeleBot("1233901660:AAF3-7hfCoS65BsMuEk5fT2EQyvJD8_RO5w")
#o segundo parametro eh a senha do servidor sql com o qual este bot vai interagir
database = Database('root', '', 'localhost', 'db_telebot_scheduler')


@bot.message_handler(commands=['aqui', 'start'])
def main_menu(message):
	name = message.from_user.first_name
	#send_welcomemsg(name)

	emoji_calendario = u"\U0001F4C5"
	emoji_cross		 = u"\U0000274C"
	emoji_lupa		 = u"\U0001F50E"

	opcoes = (
		'{}  Fazer agendamento'.format(emoji_calendario),
		'{}  Consultar agendamento(s)'.format(emoji_lupa),
		'{}  Excluir agendamento(s)'.format(emoji_cross)
	)

	markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=1)
	markup.add(*opcoes)
	bot.send_message(message.chat.id, 'Olá, {}! Com o que poderia ajudar?'.format(name), reply_markup=markup)


# opção 'fazer agendamento'
@bot.message_handler(regexp='Fazer agendamento')
def select_month(message): 
	following_months = database.get_next_months_names()

	markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=2)
	markup.add(*following_months)
	
	msg = bot.reply_to(message, 'Selecione um mês', reply_markup=markup)
	bot.register_next_step_handler(msg, select_day)

def select_day(message):
	global month
	month =  message.text
	following_days = database.get_next_days_names(month) # já na ordem, please

	markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=3)
	markup.add(*following_days)
	
	msg = bot.send_message(message.chat.id, 'Selecione um dia de {}'.format(message.text), reply_markup=markup)
	bot.register_next_step_handler(msg, select_hour)

def select_hour(message):
	following_hours = ['9:00', '10:00', '11:00', '13:00', '14:00', '15:00', '16:00', '17:00']

	global day
	day =  message.text.split(',')[0] # pega apenas o número, ignorando o nome do dia 
	#following_hours = get_hours(month, day) # já na ordem, please

	markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=3)
	markup.add(*following_hours)
	
	msg = bot.send_message(message.chat.id, 'Selecione um horário:', reply_markup=markup)
	bot.register_next_step_handler(msg, appointment_confirmation)


def appointment_confirmation(message):
	if not database.in_database(message.chat.id): #adicionar o cliente na tabela se já não estiver
		database.add_client_to_database(message.from_user.id, message.from_user.first_name, message.from_user.last_name)
	
	hour = message.text
	
	database.add_appointment(month, day, hour, message.from_user.id)
	emoji = u"\U0001f603"
	back_to_menu_message(message.chat.id, 
	"Seu agendamento foi confirmado para {} de {}, às {}! {}".format(day, month, hour, emoji)
	)


# opção 'consultar agendamento'
@bot.message_handler(regexp='Consultar')
def consult_option(message):
	chat_id = message.chat.id
	search = database.consult_appointments(chat_id)
	# verifica se existe algum agendamento
	if search == []:
		return back_to_menu_message(message.chat.id, "Você não possui agendamento.")
	search = format_consult(search)

	for line in search:
		bot.send_message(chat_id, line)
	back_to_menu_message(chat_id)


# opção 'excluir agendamento'
@bot.message_handler(regexp='Excluir')
def exclude(message):
	# pega os agendamentos no database de acordo com o chat_id
	chat_id = message.chat.id
	search = database.consult_appointments(chat_id)
	# verifica se existe algum agendamento
	if search == []:
		return back_to_menu_message(message.chat.id, "Você não possui agendamento.")
	# formata as informações para um texto mais amigável
	search = format_exclusion(search)

	# cria as opções com cada agendamento + opção de cancelar a operação
	markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=1)
	markup.add(*search)
	markup.add(red_cross_emoji()+"  cancelar exclusão")
	
	# envia texto e as opções de resposta
	msg = bot.send_message(message.chat.id, 'Selecione um agendamento para excluir:', reply_markup=markup)
	bot.register_next_step_handler(msg, exclusion_confirmation_step)

def exclusion_confirmation_step(message):
	# cancela a operação, se escolhido
	if "cancelar exclusão" in message.text:
		return back_to_menu_message(message.chat.id, "Sem problema, a operação foi cancelada. "+wink_face_emoji())

	markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, row_width=2)
	markup.add(check_mark_emoji()+"  Sim", red_cross_emoji()+"  Não")
	markup.add(red_cross_emoji()+"  cancelar exclusão")

	# tira o '- cod' da string
	appointment = message.text.split(" - ")[0]

	msg = bot.send_message(message.chat.id, 'Tem certeza que deseja excluir o agendamento para {}?'.format(appointment), reply_markup=markup)
	# segue para a próxima função, enviando o agendamento selecionado como parâmetro
	bot.register_next_step_handler(msg, lambda m: exclusion_action(m, message.text))
	
def exclusion_action(message, appointment):
	# cancela a operação, se escolhido
	if "cancelar exclusão" in message.text or "Não" in message.text:
		return back_to_menu_message(message.chat.id, "Sem problema, a operação foi cancelada. "+wink_face_emoji())

	# pega o final da string, que é exatamente o idt do agendamento
	appointment_id = appointment.split()[-1]
	database.exclude_appointment(appointment_id)

	back_to_menu_message(message.chat.id, 'Pronto! Seu agendamento foi excluído. '+wink_face_emoji())


# envia a mensagem desejada e informa como acessar o menu
def back_to_menu_message(chat_id, text=""):
	if text != "":
		bot.send_message(chat_id, text)
	bot.send_message(chat_id, 'Se quiser voltar ao menu, clique /aqui.')


# Enable saving next step handlers to file "./.handlers-saves/step.save".
# Delay=2 means that after any change in next step handlers (e.g. calling register_next_step_handler())
# saving will hapen after delay 2 seconds.
bot.enable_save_next_step_handlers(delay=2)

# Load next_step_handlers from save file (default "./.handlers-saves/step.save")
# WARNING It will work only if enable_save_next_step_handlers was called!
bot.load_next_step_handlers()

bot.polling()