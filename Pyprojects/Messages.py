#useful documentation: https://github.com/eternnoir/pyTelegramBotAPI/blob/master/README.md


def format_consult(search):
	meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]


	for line in range(len(search)):
		datetime = search[line][1]
		
		day = datetime.strftime("%d")
		month = meses[ int(datetime.strftime("%m")) - 1 ]
		hour = datetime.strftime("%H") + ':' + datetime.strftime("%M")
		search[line] = "{} de {}, às {}.".format(day, month, hour)

	return search


def format_exclusion(search):
	datetime_format = format_consult(search)

	for line in range(len(search)):
		idt = search[line][0]
		search[line] = datetime_format[line][:-1] + " - cód. {}".format(idt)

	return search


# unicode dos emojis
def wink_face_emoji():
	return u'\U0001F609'

def check_mark_emoji():
	return u'\U00002705'

def red_cross_emoji():
	return u"\U0000274C"